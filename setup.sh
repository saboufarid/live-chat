#!/bin/bash
usage()
{
  echo "Usage:" >&2
  echo " $0 [d|t|p|l]" >&2
  echo 
  echo "Environment:" >&2
  echo " d is for dev1" >&2
  echo " t is for test1" >&2
  echo " p is for prod1" >&2
  echo " x2 is for prod2" >&2
  echo " xl is for prod in London" >&2
  echo " l is for local" >&2
  echo " ldr is for local connected to dev with regular auth0" >&2
  echo " ldo is for local connected to dev with overflow auth0" >&2
  echo " ltg is for local connected to test with goofy auth0" >&2
  echo " lto is for local connected to test with overflow auth0" >&2
  echo 
  exit 1
}

if [[ "$#" -eq 0 || "$#" -gt 1 ]]; then
	usage
fi

if ! [[ "$1" =~ ^(d|dev(1)?|t|test(1)?|p|prod(1)?|l|local|x2|xl|ldr|ldo|ltg|lto)$ ]]; then
	echo "Unknown environment $1" >&2
	usage
fi

case "$1" in
	d|dev|dev1)		env="dev1";;
	t|test|test1)	env="test1";;
	p|prod|prod1) env="prod1";;
	xl) 					env="plndn";;
	x2) 					env="prod2";;
	l|local)			env="local";;
	ldr)	cp _env.local.dev.regular _env.local
				cp auth_config.json.local.dev.regular auth_config.json.local
				env="local";;
	ldo)	cp _env.local.dev.overflow _env.local
				cp auth_config.json.local.dev.overflow auth_config.json.local
				env="local";;
	ltg)	cp _env.local.test.goofy _env.local
				cp auth_config.json.local.test.goofy auth_config.json.local
				env="local";;
	lto)	cp _env.local.test.overflow _env.local
				cp auth_config.json.local.test.overflow auth_config.json.local
				env="local";;
	*) usage;;
esac

echo "Setup $env" >&2

sed "s/^REACT_APP_BORN=.*/REACT_APP_BORN='$(date)'/" _env.$env > .env
cp manifest.$env.yml manifest.yml
cp auth_config.json.$env src/auth_config.json

if [[ "$env" == "local" ]]; then
	rm manifest.yml
	npm run build
fi
