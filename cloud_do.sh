#!/bin/bash
usage()
{
  echo "Usage:" >&2
  echo " $0 [d|t|p] [push|ssh|logs|recent|stop|start|restart]" >&2
  echo 
  echo "Environment:" >&2
  echo " d is for dev1" >&2
  echo " t is for test1" >&2
  echo " p is for prod1" >&2
  echo " l is for local" >&2
  echo 
  echo "Action, one in:" >&2
	echo " push, ssh, logs, stop, start, restart" >&2
  echo 
  exit 1
}

if [[ "$#" -eq 0 || "$#" -gt 2 ]]; then
	usage
fi

if ! [[ "$1" =~ ^(d|dev(1)?|t|test(1)?|p|prod(1)?|l|local)$ ]]; then
	echo "Unknown environment $1" >&2
	usage
fi

if [[ "$#" -eq 2 ]]; then
	if ! [[ "$2" =~ ^(push|pushprocess|ssh|logs|recent|stop|start|restart)$ ]]; then
		echo "Unknown command $2" >&2
		usage
	fi
fi

echo
./setup.sh "$1"
echo

case "$1" in
	d|dev|dev1)
		env="dev1"
		API_ENDPOINT="api.eu-gb.bluemix.net"
		EMAIL="corpus@dev1.faibrik.com"
		PASW="Mot3Passe!"
		SPACE="dev"
		# cf curl /v2/info
		SSH_ENDPOINT="ssh.eu-gb.cf.cloud.ibm.com"
		# cf app livechat-client-dev1 --guid
		SSH_USER="cf:.../0"
		;;
	t|test|test1)
		env="test1"
		API_ENDPOINT="api.eu-gb.bluemix.net"
		EMAIL="corpus@test1.faibrik.com"
		PASW="Mot3Passe!"
		SPACE="dev"
		# cf curl /v2/info
		SSH_ENDPOINT="ssh.eu-gb.cf.cloud.ibm.com"
		# cf app livechat-client-test1 --guid
		SSH_USER="cf:.../0"
		;;
	p|prod|prod1)
		echo
		echo "---> WRONG SCRIPT"
		echo "Remember to use cf version 6.49.0"
		echo
		exit 1
		;;
	l|local) usage;;
	*) usage;;
esac

ORG=$EMAIL
F_APP="livechat-client-$env"

cf login -a $API_ENDPOINT -u $EMAIL -o $ORG -s $SPACE -p $PASW

case "$2" in
	stop)			cf stop $F_APP;;
	start)		cf start $F_APP;;
	restart)	cf restart $F_APP;;
	push)			npm run build
						cf push $F_APP
						;;
	pushprocess)    cf set-env $F_APP FQN $F_APP-$API_ENDPOINT
                    cf push $F_APP -u process;;
	logs)			cf logs $F_APP;;
	recent)		cf logs $F_APP --recent;;
	ssh) cf ssh-code
			 scp -P 2222 -r -o User=$SSH_USER $SSH_ENDPOINT:/home/vcap/app/logs/ BMX_logs/
			 ;;
	*) echo
		 echo "Connected ... waiting commands"
		 echo
		 cf apps;;
esac