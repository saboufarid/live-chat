import React from "react";
import { Flex, Link } from "theme-ui";

const FaibrikBranding = () => {
	return (
		<Flex m={2} sx={{ justifyContent: "center", alignItems: "center" }}>
			<Link
				href="https://www.faibrik.com"
				target="_blank"
				rel="noopener noreferrer"
				sx={{
					color: "gray",
					opacity: 0.8,
					transition: "0.2s",
					"&:hover": { opacity: 1 }
				}}
			>
				Réalisé par fAibrik avec ❤️
			</Link>
		</Flex>
	);
};

export default FaibrikBranding;
