class Logger {
  debugModeEnabled: boolean;

  constructor(debugModeEnabled?: boolean) {
    this.debugModeEnabled = !!debugModeEnabled;
  }

  debug(...args: any) {
    if (!this.debugModeEnabled) {
      return;
    }

    console.debug('[Faibrik iframe]', ...args);
  }

  log(...args: any) {
    if (!this.debugModeEnabled) {
      return;
    }

    console.log('[Faibrik iframe]', ...args);
  }

  info(...args: any) {
    console.info('[Faibrik iframe]', ...args);
  }

  warn(...args: any) {
    console.warn('[Faibrik iframe]', ...args);
  }

  error(...args: any) {
    console.error('[Faibrik iframe]', ...args);
  }
}

export default Logger;
